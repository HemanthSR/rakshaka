var Rakshaka = angular.module("rakshaka", ["ionic", "firebase", "angularSpinners"]);
Rakshaka.run(function ($ionicPlatform, $state) {
    $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
    document.addEventListener("resume", function () {
        $state.go("locked", {}, {location: "replace"});
    }, false);
});

Rakshaka.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state("locked", {
            url: "/locked",
            templateUrl: "templates/locked.html",
            controller: "VaultController",
            cache: false,
            resolve: {
                fb: function () {
                    return new Firebase("https://rakshaka.firebaseio.com/");
                }
            }
        })
        .state("createvault", {
            url: "/createvault",
            templateUrl: "templates/create_vault.html",
            controller: "VaultController",
            resolve: {
                fb: function () {
                    return new Firebase("https://rakshaka.firebaseio.com/");
                }
            }
        })
        .state("firebase", {
            url: "/firebase",
            templateUrl: "templates/firebase.html",
            controller: "FirebaseController",
            resolve: {
                fb: function () {
                    return new Firebase("https://rakshaka.firebaseio.com/");
                }
            }
        })
        .state("categories", {
            url: "/categories/:masterPassword",
            templateUrl: "templates/categories.html",
            controller: "CategoryController",
            resolve: {
                fb: function () {
                    return new Firebase("https://rakshaka.firebaseio.com/");
                }
            }
        })
        .state("accounts", {
            url: "/accounts/:categoryId/:masterPassword",
            templateUrl: "templates/accountsList.html",
            controller: "AccountController",
            cache: false,
            resolve: {
                fb: function () {
                    return new Firebase("https://rakshaka.firebaseio.com/");
                }
            }
        });
    $urlRouterProvider.otherwise('/locked');
    $.material.init();
});

Rakshaka.factory("$cipherFactory", function () {

    return {

        /*
         * Encrypt a message with a passphrase or password
         *
         * @param    string message
         * @param    string password
         * @return   object
         */
        encrypt: function (message, password) {
            var salt = forge.random.getBytesSync(128);
            var key = forge.pkcs5.pbkdf2(password, salt, 4, 16);
            var iv = forge.random.getBytesSync(16);
            var cipher = forge.cipher.createCipher('AES-CBC', key);
            cipher.start({iv: iv});
            cipher.update(forge.util.createBuffer(message));
            cipher.finish();
            var cipherText = forge.util.encode64(cipher.output.getBytes());
            return {cipher_text: cipherText, salt: forge.util.encode64(salt), iv: forge.util.encode64(iv)};
        },

        /*
         * Decrypt cipher text using a password or passphrase and a corresponding salt and iv
         *
         * @param    string (Base64) cipherText
         * @param    string password
         * @param    string (Base64) salt
         * @param    string (Base64) iv
         * @param    object
         * @return   string
         */
        decrypt: function (cipherText, password, salt, iv, options) {
            var key = forge.pkcs5.pbkdf2(password, forge.util.decode64(salt), 4, 16);
            var decipher = forge.cipher.createDecipher('AES-CBC', key);
            decipher.start({iv: forge.util.decode64(iv)});
            decipher.update(forge.util.createBuffer(forge.util.decode64(cipherText)));
            decipher.finish();
            if (options !== undefined && options.hasOwnProperty("output") && options.output === "hex") {
                return decipher.output.toHex();
            } else {
                return decipher.output.toString();
            }
        }

    };

});

String.prototype.toHex = function () {
    var buffer = forge.util.createBuffer(this.toString());
    return buffer.toHex();
};

String.prototype.toSHA1 = function () {
    var md = forge.md.sha1.create();
    md.update(this);
    return md.digest().toHex();
};
