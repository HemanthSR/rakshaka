Rakshaka.controller("VaultController", function ($scope, $state, $ionicHistory, $firebaseObject, $cipherFactory, $ionicLoading, $ionicPopup, fb, spinnerService) {

    $ionicHistory.clearHistory();

    $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });

    var fbAuth = fb.getAuth();
    if (fbAuth) {
        var userReference = fb.child("rakshaka/" + fbAuth.uid);
        var syncObject = $firebaseObject(userReference);
        syncObject.$bindTo($scope, "data");
    } else {
        $state.go("firebase");
    }

    $scope.unlock = function (masterPassword) {
        spinnerService.show('spinner');
        syncObject.$loaded().then(function () {
            var decipherPhrase = $cipherFactory.decrypt($scope.data.masterPassword.cipher_text, masterPassword, $scope.data.masterPassword.salt, $scope.data.masterPassword.iv, {output: "hex"});
            if (decipherPhrase === "Authenticated".toHex()) {
                $state.go("categories", {masterPassword: masterPassword});
            }
            spinnerService.hide('spinner');
        });
    };

    $scope.create = function (masterPassword) {
        spinnerService.show('spinner');
        syncObject.$loaded().then(function () {
            userReference.child("masterPassword").set($cipherFactory.encrypt("Authenticated", masterPassword), function () {
                $state.go("locked");
            });
            spinnerService.hide('spinner');
        });
    };

    $scope.reset = function () {
        $ionicPopup.confirm({
            title: 'Reset Data',
            template: "Are you sure you want to delete all data?"
        }).then(function (result) {
            if (result) {
                spinnerService.show('spinner');
                userReference.remove(function (error) {
                    if (error) {
                        spinnerService.hide('spinner');
                        console.error("ERROR: " + error);
                    } else {
                        spinnerService.hide('spinner');
                        $state.go("createvault");
                    }
                });
            }
        })
    }
});