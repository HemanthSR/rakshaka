Rakshaka.controller("CategoryController", function ($scope, $ionicPopup, $firebaseObject, $stateParams, $cipherFactory, $ionicLoading, fb, spinnerService) {

    $scope.masterPassword = $stateParams.masterPassword;
    $scope.categories = [];

    $scope.newCategory = {};

    var fbAuth = fb.getAuth();
    if (fbAuth) {
        var categoriesReference = fb.child("rakshaka/" + fbAuth.uid);
        var syncObject = $firebaseObject(categoriesReference);
        syncObject.$bindTo($scope, "data");
    } else {
        $state.go("firebase");
    }

    $scope.list = function () {
        spinnerService.show('spinner');
        syncObject.$loaded().then(function () {
            for (var key in $scope.data.categories) {
                if ($scope.data.categories.hasOwnProperty(key)) {
                    $scope.categories.push({
                        id: key,
                        name: $cipherFactory.decrypt($scope.data.categories[key].category.name.cipher_text, $stateParams.masterPassword, $scope.data.categories[key].category.name.salt, $scope.data.categories[key].category.name.iv),
                        description: $cipherFactory.decrypt($scope.data.categories[key].category.description.cipher_text, $stateParams.masterPassword, $scope.data.categories[key].category.description.salt, $scope.data.categories[key].category.description.iv)
                    });
                }
            }
            spinnerService.hide('spinner');
        });
    };

    $scope.add = function () {
        if ($scope.newCategory !== undefined && $scope.newCategory.name != undefined) {
            if ($scope.data.categories === undefined) {
                $scope.data.categories = {};
            }
            if ($scope.data.categories[$scope.newCategory.name.toSHA1()] === undefined) {
                $scope.data.categories[$scope.newCategory.name.toSHA1()] = {
                    category: {
                        name: $cipherFactory.encrypt($scope.newCategory.name, $stateParams.masterPassword),
                        description: $cipherFactory.encrypt($scope.newCategory.description, $stateParams.masterPassword)
                    },
                    accounts: {}
                };
                $scope.categories.push({
                    id: $scope.newCategory.name.toSHA1(),
                    name: $scope.newCategory.name,
                    description: $scope.newCategory.description
                });
                $scope.newCategory = {};
            }
        }
    }
});
