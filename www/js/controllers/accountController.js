Rakshaka.controller("AccountController", function ($scope, $stateParams, $firebaseObject, $state, $cipherFactory, $ionicHistory, $ionicLoading, fb, spinnerService) {

    $scope.masterPassword = $stateParams.masterPassword;
    $scope.categoryId = $stateParams.categoryId;
    $scope.accounts = [];
    $scope.newAccount = {
        title: undefined,
        username: undefined,
        password: undefined
    };

    var fbAuth = fb.getAuth();
    if (fbAuth) {
        var categoryReference = fb.child("rakshaka/" + fbAuth.uid + "/categories/" + $stateParams.categoryId);
        var accountsReference = fb.child("rakshaka/" + fbAuth.uid + "/categories/" + $stateParams.categoryId + "/accounts");
        var syncObject = $firebaseObject(categoryReference);
        syncObject.$bindTo($scope, "data");
    } else {
        $state.go("firebase");
    }

    $scope.list = function () {
        spinnerService.show('spinner');
        $scope.accounts = [];
        syncObject.$loaded().then(function () {
            var encryptedAccounts = $scope.data.accounts;
            for (var key in encryptedAccounts) {
                if (encryptedAccounts.hasOwnProperty(key)) {
                    $scope.accounts.push({
                        id: key,
                        account: JSON.parse($cipherFactory.decrypt(encryptedAccounts[key].cipher_text, $stateParams.masterPassword, encryptedAccounts[key].salt, encryptedAccounts[key].iv))
                    });
                }
            }
            spinnerService.hide('spinner');
        });
    };

    $scope.delete = function (accountID) {
        spinnerService.show('spinner');
        accountsReference.child(accountID).remove(function () {
            $scope.list();
            spinnerService.hide('spinner');
        });
    };

    $scope.save = function () {
        spinnerService.show('spinner');
        if ($scope.editingAccountId) {
            accountsReference.child($scope.editingAccountId).remove(function () {
                spinnerService.hide('spinner');
            });
            $scope.editingAccountId = undefined;
            saveNewAccount();
        } else {
            saveNewAccount();
        }
    };

    var saveNewAccount = function () {
        spinnerService.show('spinner');
        accountsReference.child(JSON.stringify($scope.newAccount).toSHA1()).set($cipherFactory.encrypt(JSON.stringify($scope.newAccount), $stateParams.masterPassword), function () {
            $scope.newAccount = undefined;
            spinnerService.hide('spinner');
            $scope.list();
        });
    };

    $scope.edit = function (account) {
        $scope.editingAccountId = account.id;
        $scope.newAccount = account.account;
    };

    function randomString(len) {
        var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    }

    $scope.generatePassword = function () {
        $scope.newAccount.password = randomString(10);
    };

    $scope.back = function () {
        $ionicHistory.goBack();
    };

    var init = function () {
        $scope.list();
    };

    init();

});
