Rakshaka.controller("FirebaseController", function ($scope, $state, $ionicHistory, $firebaseAuth, $ionicLoading, fb, $ionicPopup, spinnerService) {
    $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
    });

    var fbAuth = $firebaseAuth(fb);

    $scope.login = function (username, password) {
        spinnerService.show('spinner');
        fbAuth.$authWithPassword({
            email: username,
            password: password
        }).then(function () {
            spinnerService.hide('spinner');
            $state.go("locked");
        }).catch(function (error) {
            spinnerService.hide('spinner');
            $ionicPopup.alert({title: "Authentication Failed.", template: error});
        });
    };

    $scope.register = function (username, password) {
        spinnerService.show('spinner');
        fbAuth.$createUser({email: username, password: password}).then(function () {
            return fbAuth.$authWithPassword({
                email: username,
                password: password
            });
        }).then(function () {
            spinnerService.hide('spinner');
            $state.go("createvault");
        }).catch(function (error) {
            spinnerService.hide('spinner');
            $ionicPopup.alert({title: "Registration Failed.", template: error});
        });
    }

});
